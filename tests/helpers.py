"""Test Helpers."""

from brewdisplay.models.recipe import Recipe

def load_recipes():
    """Load recipes from disk."""
    recipes = Recipe.load_from_disk('tests/files/')
    return recipes

def load_a_recipe():
    """Load a single recipe from disk."""
    with open('tests/files/VanillaCreamAle.xml', 'r') as file_open:
        recipe = Recipe.from_beerxml(file_open.read(), save_as='test')
    return recipe
