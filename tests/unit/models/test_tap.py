"""Tests for brewdisplay.models.tap."""

from brewdisplay.extensions import db
from brewdisplay.models.tap import Tap
from tests.unit.base_test_cases import BaseAppTest
from tests.helpers import load_a_recipe

# pylint: disable=no-member

class TestPuttingRecipeOnTap(BaseAppTest):
    """Tests for the relationship between a Tap and a Recipe."""

    def setUp(self):
        """Method setupd."""
        super().setUp()
        self.tap_one = Tap(tap_number=1)
        self.tap_two = Tap(tap_number=2)
        db.session.add(self.tap_one)
        db.session.add(self.tap_two)
        db.session.commit()

    def test_put_beer_on_tap(self):
        """A recipe is able to be assocaited with a tap."""
        recipe = load_a_recipe()
        recipe.on_tap = self.tap_one
        db.session.add(recipe)
        db.session.commit()
