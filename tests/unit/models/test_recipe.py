"""Tests for brewdisplay.models.recipe."""

from tests.unit.base_test_cases import BaseAppTest
from tests.helpers import load_a_recipe

from brewdisplay.models.rating import Rating
from brewdisplay.extensions import db

# pylint: disable=no-member

class TestRecipeCreation(BaseAppTest):
    """Tests for creating a recipe model."""

    def test_recipe_parse(self):
        """A recipe is parsable from beerXML format."""
        recipe = load_a_recipe()
        db.session.add(recipe)
        db.session.commit()
        self.assertEqual(recipe.beerxml_file, 'test')
        self.assertEqual(recipe.name, 'Vanilla Cream Ale')
        self.assertEqual(recipe.on_tap, None)

class TestRecipeRatings(BaseAppTest):
    """Tests for creating a Recipe Rating."""

    def setUp(self):
        """Setup test."""
        super().setUp()
        self.recipe = load_a_recipe()
        db.session.add(self.recipe)
        db.session.commit()

    def test_recipe_ratings(self):
        """The average rating is built from the recipes ratings."""
        for i in range(5):
            recipe = Rating(recipe_id=self.recipe.id, rating=i)
            db.session.add(recipe)
            db.session.commit()

        self.assertEqual(self.recipe.average_rating, 2)

    def test_recipe_without_ratings(self):
        """Recipes without ratings have an average rating of 0."""
        self.assertEqual(len(self.recipe.ratings), 0)
        self.assertEqual(self.recipe.average_rating, 0)
