"""Tests for interactions with taps."""

from flask import url_for

from tests.unit.base_test_cases import BaseClientTest


class TestAddTaps(BaseClientTest):
    """Tests for adding a tap."""

    def setUp(self):
        """Setup for a tap."""
        super().setUp()
        self._endpoint = url_for('api_v1_taps.Taps')

    def test_create_tap(self):
        """A created tap returns a 201 status code."""
        response = self.client.post(self._endpoint)
        self.assertEqual(response.status_code, 201)

    def test_create_tap_and_retrieve(self):
        """Test the creation and retrieval of a single tap."""
        self.client.post(self._endpoint)
        response = self.client.get(self._endpoint)
        self.assertEqual(len(response.json), 1)
        self.assertEqual(response.status_code, 200)

    def test_create_multiple_taps(self):
        """Multiple calls to POST taps create a tap."""
        self.client.post(self._endpoint)
        self.client.post(self._endpoint)
        self.client.post(self._endpoint)
        self.client.post(self._endpoint)
        response = self.client.get(self._endpoint)
        self.assertEqual(len(response.json), 4)
        self.assertEqual(response.status_code, 200)


class TestDeleteTaps(BaseClientTest):
    """Tests for deleting a tap."""

    def setUp(self):
        """Environment Setup."""
        super().setUp()
        self._endpoint = url_for('api_v1_taps.Taps')
        self.client.post(self._endpoint)
        self.client.post(self._endpoint)
        self.client.post(self._endpoint)
        self.client.post(self._endpoint)

    def test_deleted_tap_is_last_tap(self):
        """After adding multiple taps, a delete will remove the newest."""
        response = self.client.get(self._endpoint)
        taps = response.json
        highest_tap = max([t['tap_number'] for t in taps])
        number_of_taps = len(taps)
        self.client.delete(self._endpoint)
        new_response = self.client.get(self._endpoint)
        new_taps = new_response.json
        number_taps_new = len(new_taps)
        new_highest_tap = max([t['tap_number'] for t in new_taps])
        self.assertEqual(new_highest_tap, highest_tap-1)
        self.assertEqual(number_of_taps - 1, number_taps_new)
