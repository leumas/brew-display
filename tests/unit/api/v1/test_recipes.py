"""Test the recipe views."""

from functools import partial

from flask import url_for

from tests.unit.base_test_cases import BaseClientTest
from tests.helpers import load_recipes, load_a_recipe

from brewdisplay.extensions import db
from brewdisplay.models.recipe import Recipe

class TestGetRecipes(BaseClientTest):
    """Tests for GET endpoints for recipes."""

    def setUp(self):
        """Setup the environment."""
        super().setUp()
        self.endpoint = url_for('api_v1_recipes.Recipes')
        self.id_endpoint = partial(url_for, 'api_v1_recipes.RecipeById')
        self.download_endpoint = partial(url_for, 'api_v1_recipes.RecipeDownload')
        self.recipes = load_recipes()

    def test_get_recipes(self):
        """Recipe collection 200 test."""
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json), len(self.recipes))

    def test_get_recipe(self):
        """Recipe by ID 200."""
        response = self.client.get(
            self.id_endpoint(recipe_id=self.recipes[0].id))
        self.assertEqual(response.status_code, 200)

    def test_recipe_404(self):
        """When a recipe isn't found, the status code is 404."""
        response = self.client.get(
            self.id_endpoint(recipe_id=1000000))
        self.assertEqual(response.status_code, 404)

    def test_recipe_download_404(self):
        """When the recipe file is not found, the status code is 404."""
        response = self.client.get(
            self.download_endpoint(recipe_id=1000000))
        self.assertEqual(response.status_code, 404)


class TestRateRecipe(BaseClientTest):
    """Tests for rating a recipe."""

    def setUp(self):
        """Environment setup."""
        super().setUp()
        self.endpoint = partial(url_for, 'api_v1_recipes.RecipeRatings')
        self.recipe = load_a_recipe()
        # pylint: disable=no-member
        db.session.add(self.recipe)
        db.session.commit()

    def test_rate_a_recipe(self):
        """Test the rating of a recipe."""
        self.assertEqual(len(self.recipe.ratings), 0)
        response = self.client.post(
            self.endpoint(recipe_id=self.recipe.id, rating=5))
        self.assertEqual(response.status_code, 201)
        # Refresh our item, since it was modified
        recipe = Recipe.query.filter_by(id=self.recipe.id).first()
        self.assertEqual(len(recipe.ratings), 1)

class TestUploadRecipe(BaseClientTest):
    """Tests for uploading a recipe."""

    def setUp(self):
        """Environment setup."""
        super().setUp()
        self.endpoint = url_for('api_v1_recipes_upload.recipe_create_from_xml')
        self.recipe = open('tests/files/VanillaCreamAle.xml', 'rb')

    def test_post_a_recipe(self):
        """POST a recipe with form data."""
        data = {
            'file': (self.recipe, 'test_recipe.xml'),
        }
        response = self.client.post(
            self.endpoint,
            data=data,
            content_type='multipart/form-data',
        )
        self.assertEqual(response.status_code, 302)

    def tearDown(self):
        """Teardown."""
        self.recipe.close()
