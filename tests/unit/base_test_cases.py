"""Base test cases for reuse."""

import unittest

from brewdisplay.application import create_app
from brewdisplay.config import TestConfig
from brewdisplay.extensions import db


class BaseAppTest(unittest.TestCase):
    """An application testcase.

    This should be used to set up an application context in flask.
    """

    def setUp(self):
        """Environment setup.

        Create an application and push the context. Set up any additional
        items that are needed for the application to run.
        """
        self.app = create_app(config_override=TestConfig)
        self._context = self.app.app_context()
        self._context.push()
        db.create_all()

    def tearDown(self):
        """Teardown the environment.

        Pop off the context of the application.
        """
        self._context.pop()

class BaseClientTest(BaseAppTest):
    """Base test case that requires an application client."""

    def setUp(self):
        """Environment setup."""
        super().setUp()
        self.client = self.app.test_client()
