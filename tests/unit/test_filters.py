"""Tests for application filters."""

import unittest

from brewdisplay.filters import srm_float_to_color, HEX_COLORS

class TestSRMtoFloat(unittest.TestCase):
    """Tests brewdisplay.filters.srm_float_to_color."""

    def test_black(self):
        """A large SRM will return the highest SRM color defined."""
        hex_color = srm_float_to_color(100.34)
        self.assertEqual(hex_color, HEX_COLORS[-1])

    def test_lower_than_one(self):
        """A small SRM will return the first item."""
        hex_color = srm_float_to_color(0.231)
        self.assertEqual(hex_color, HEX_COLORS[0])

    def test_middle(self):
        """A mid number SRM will return the proper indexed hex color."""
        hex_color = srm_float_to_color(5.4)
        self.assertEqual(hex_color, HEX_COLORS[5])
