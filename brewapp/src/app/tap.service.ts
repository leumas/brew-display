import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Tap } from './tap';
import { MessageService } from './message.service';
import { Recipe } from './recipe';


@Injectable({
  providedIn: 'root'
})
export class TapService {
  private tapsUrl = 'http://localhost:5000/api/v1/taps/'
  private servingUrl = 'http://localhost:5000/api/v1/serve/'

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  getTaps (): Observable<Tap[]> {
    return this.http.get<Tap[]>(this.tapsUrl).pipe(
      map(taps => taps));
  }

  getTap (id: number): Observable<Tap> {
    const url = `${this.tapsUrl}${id}`;
    return this.http.get<Tap>(url);
  }

  createTap (): Observable<Tap> {
    return this.http.post<Tap>(this.tapsUrl, null);
  }

  deleteTap (): Observable<any> {
    return this.http.delete(this.tapsUrl).pipe(
      catchError(this.handleError<Tap>('deleteTap'))
    );
  }

  serveRecipe (recipeId: number, tapId: number): Observable<any> {
    const url = `${this.tapsUrl}${tapId}/serves/${recipeId}`
    return this.http.put(url, null);
  }

  removeServing (tapId: number): Observable<any> {
    const url = `${this.tapsUrl}${tapId}/serves/`
    return this.http.delete(url)

  }

  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`);
  }

  // XXX This is duplicate of the recipe service.
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

