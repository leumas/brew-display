import {Recipe} from './recipe';

export class Tap {
    id: number;
    tap_number: number;
    serving: Recipe;
    created: string;
    on_tap: boolean;
    description: string;
    srm: number;
    abv: number;
}