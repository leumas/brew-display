import { TestBed, inject } from '@angular/core/testing';

import { RecipeUploadService } from './recipe-upload.service';

describe('RecipeUploadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecipeUploadService]
    });
  });

  it('should be created', inject([RecipeUploadService], (service: RecipeUploadService) => {
    expect(service).toBeTruthy();
  }));
});
