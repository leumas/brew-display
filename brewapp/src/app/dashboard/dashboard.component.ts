import { Component, OnInit } from '@angular/core';
import { Tap } from '../tap';
import { TapService } from '../tap.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  taps: Tap[] = [];

  constructor(private tapService: TapService) { }

  ngOnInit() {
    this.getRecipes();
  }

  getRecipes(): void {
    this.tapService.getTaps()
      .subscribe(taps => this.taps = taps);
  }
}