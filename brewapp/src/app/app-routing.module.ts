import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipesComponent }      from './recipes/recipes.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { RecipeDetailComponent }  from './recipe-detail/recipe-detail.component';
import { TapsComponent } from './taps/taps.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'recipes', component: RecipesComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'recipes/:id', component: RecipeDetailComponent },
  { path: 'taps', component: TapsComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
