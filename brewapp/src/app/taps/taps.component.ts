import { Component, OnInit } from '@angular/core';
import { Tap } from '../tap';
import { TapService } from '../tap.service'
import { RecipeService } from '../recipe.service';
import { Recipe } from '../recipe';

@Component({
  selector: 'app-taps',
  templateUrl: './taps.component.html',
  styleUrls: ['./taps.component.css']
})
export class TapsComponent implements OnInit {
  taps: Tap[];
  possibleRecipes: Recipe[];

  constructor(
    private tapService: TapService,
    private recipeService: RecipeService) { }

  ngOnInit() {
    this.getTaps();
    this.getRecipes();
  }

  delete(): void {
    this.tapService.deleteTap().subscribe();
    // This should likely filter the current taps instead
    // of re-retrieving
    this.taps.pop()
  }

  getTaps(): void {
    this.tapService.getTaps().subscribe(
      taps => this.taps = taps);
  }

  getRecipes(): void {
    this.recipeService.getRecipes().subscribe(
      recipes => this.possibleRecipes = recipes);
  }

  serveRecipe(recipeId: number, tapId: number): void {
    if (recipeId == 0) {
      this.tapService.removeServing(tapId).subscribe()
    }
    else {
      this.tapService.serveRecipe(recipeId, tapId).subscribe();
    }
  }

  add(): void {
    this.tapService.createTap().subscribe(
      tap => {this.taps.push(tap)});
  }
}
