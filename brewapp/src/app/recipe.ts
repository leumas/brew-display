export class Recipe {
    id: number;
    name: string;
    average_rating: number;
    created: string;
    on_tap: boolean;
    description: string;
    srm: number;
    abv: number;
}