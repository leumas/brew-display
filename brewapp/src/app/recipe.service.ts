import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map, tap, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { Recipe } from './recipe';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  private recipesUrl = 'http://localhost:5000/api/v1/recipes/'

  constructor(
    private messageService: MessageService,
    private http: HttpClient) { }

  getRecipes (): Observable<Recipe[]> {
    return this.http.get<Recipe[]>(this.recipesUrl).pipe(
      map(recipes => recipes))
  }
  getRecipe(id: number): Observable<Recipe> {
    this.messageService.add(`RecipeService: fetched recipe ${id}`);
    const url = `${this.recipesUrl}${id}`;
    return this.http.get<Recipe>(url)
  }

  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`);
  }

  updateRecipe(recipe: Recipe): Observable<any> {
    const url = `${this.recipesUrl}${recipe.id}`
    return this.http.put(url, recipe, httpOptions).pipe(
      tap(_ => this.log(`updated recipe id ${recipe.id}`)),
      catchError(this.handleError<any>('updateRecipe'))
    )
  }

  addRecipe (recipe: Recipe): Observable<Recipe> {
    return this.http.post<Recipe>(this.recipesUrl, recipe, httpOptions).pipe(
      tap((recipe: Recipe) => this.log(`added recipe with id=${recipe.id}`)),
      catchError(this.handleError<Recipe>('addRecipe'))
    );
  }

  deleteRecipe(recipe: Recipe): Observable<any> {
    const id = typeof recipe === 'number' ? recipe : recipe.id;
    this.messageService.add(`RecipeService: deleted recipe ${id}`);
    const url = `${this.recipesUrl}${id}`
    return this.http.delete<Recipe>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted recipe id=${id}`)),
      catchError(this.handleError<Recipe>('deleteRecipe'))
    );
  }

    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  uploadBeerXML(recipeFile: File): Observable<any> {
    let formData:FormData = new FormData();
    formData.append('file', recipeFile, recipeFile.name);
    const options = {
      headers: { 'Content-Type': 'multipart/form-data' }
    };
    const url = `${this.recipesUrl}beerxml`;
    return this.http.post<Recipe>(url, formData, options).pipe(
      tap(_ => this.log(`create beerxml`)),
      catchError(this.handleError<Recipe>('uploadRecipe'))
    );
  }
}
