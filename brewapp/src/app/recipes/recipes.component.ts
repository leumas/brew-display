import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {
  recipes: Recipe[];
  create: boolean;
  viewing: boolean;
  uploading: boolean;
  uploadUrl: string;

  constructor(private recipeService: RecipeService) {}

  ngOnInit() {
    this.getRecipes();
    this.create = false;
    this.viewing = true;
    this.uploading = false
  }

  add(name: string, description: string, abv: number, srm: number): void {
    name = name.trim();
    description = description.trim();
    if (!name || !description) { return; }
    this.recipeService.addRecipe({name, description, abv, srm} as Recipe)
      .subscribe(recipe => {
        this.recipes.push(recipe);
      });
  }

  onChange(files) {
    console.log(files);
    this.recipeService.uploadBeerXML(files[0])
      .subscribe(recipe => {
        this.recipes.push(recipe);
      });
  }

  toggleCreate(): void {
    if (this.create) {
      this.create = false;
    }
    else {
      this.create = true;
    }
  }

  toggleViewing(): void {
    if (this.viewing) {
      this.viewing = false;
    }
    else {
      this.viewing = true;
    }
  }

  toggleUpload(): void {
    if (this.uploading) {
      this.uploading = false;
    }
    else {
      this.uploading = true;
    }
  }

  delete(recipe: Recipe): void {
    this.recipes = this.recipes.filter(r => r !== recipe);
    this.recipeService.deleteRecipe(recipe).subscribe();
  }

  getRecipes(): void {
    this.recipeService.getRecipes()
      .subscribe(recipes => this.recipes = recipes);
  }
}

