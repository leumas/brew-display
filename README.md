Brew Display
============
Display your home taps by uploading Beerxml files.

Features
========
* Upload your recipes and put them on tap!
* Keep a historical record of previous recipes
* Friends can download the recipe directly

Roadmap
=======
* User ratings - have your friends rate your beer!
* Comments and tasting notes on specific recipes
* (stretch) keg levels!

Installation
============
TODO


Development
===========

Install pipenv and python3.

Install Node Package Manager and Angular CLI

```
sudo npm install -g @angular/cli
```

Run the backend `$ make run`

Run the frontend `$ make run-angular`

Backend Tests `$ make test`
