"""Application factory for brewdisplay."""
import os

from flask import Flask

import brewdisplay.api.v1.recipes
import brewdisplay.api.v1.taps
import brewdisplay.api.v1.recipe_upload

from brewdisplay.config import Config
from brewdisplay.extensions import db, api, marshmallow
from brewdisplay.filters import register_filters


def create_app(config_override=None):
    """Brewdisplay application factory.

    Construct an application ready for requests.

    :param test_config: Override the application with a config.
    :type test_config: brewdisplay.config.Config
    :return: The application.
    :rtype: flask.Flask
    """
    # create and configure the app
    app = Flask('brewdisplay', instance_relative_config=True)

    # Use a test app if supplied.
    if config_override is None:
        app.config.from_object(Config)
    else:
        app.config.from_object(config_override)

    register_filters(app)
    db.init_app(app)
    marshmallow.init_app(app)
    api.init_app(app)

    with app.app_context():
        db.create_all()

    # Due to the spec generation of flask-rest-api
    # the imports need to live here. Not beautiful.
    register_blueprints(app)

    if app.config.get('DEV'):
        # pylint: disable=unused-variable
        @app.after_request
        def after_request(response):
            response.headers.add(
                'Access-Control-Allow-Origin', '*')
            response.headers.add(
                'Access-Control-Allow-Headers', 'Content-Type,Authorization')
            response.headers.add(
                'Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
            return response

    # Create the recipes folder if it doesn't exist
    if not os.path.exists(app.config['RECIPE_FOLDER']):
        os.makedirs(app.config['RECIPE_FOLDER'])

    return app


def register_blueprints(application):
    """Register the api blueprints onto the application."""
    application.register_blueprint(brewdisplay.api.v1.recipes.blueprint)
    application.register_blueprint(brewdisplay.api.v1.taps.blueprint)
    application.register_blueprint(brewdisplay.api.v1.recipe_upload.blueprint)
