"""Template filter module.

XXX This will become defunct once angular becomes frontend.
"""

import math

HEX_COLORS = [
    # If SRM is < 1 choose this
    '#F3F993',
    # If SRM is between 1 and 2 choose this.
    '#F5F75C',
    '#F6F513',
    '#EAE615',
    '#E0D01B',
    '#D5BC26',
    '#CDAA37',
    '#C1963C',
    '#BE8C3A',
    '#BE823A',
    '#C17A37',
    '#BF7138',
    '#BC6733',
    '#B26033',
    '#A85839',
    '#985336',
    '#8D4C32',
    '#7C452D',
    '#6B3A1E',
    '#5D341A',
    '#4E2A0C',
    '#4A2727',
    '#361F1B',
    '#261716',
    '#231716',
    '#19100F',
    '#16100F',
    '#120D0C',
    '#100B0A',
    '#050B0A',
]


def register_filters(application):
    """Register custom filters on an application."""
    # pylint: disable=unused-variable
    @application.template_filter()
    def srm_to_hex_color(srm_float):
        """Retrieve a hex color code from a SRM float number.

        This list of hex codes was taken from:
        https://www.homebrewtalk.com/forum/threads/ebc-or-srm-to-color-rgb.78018/
        """
        return srm_float_to_color(srm_float)


def srm_float_to_color(srm_float):
    """Return a hex color based on the SRM float value.

    :param srm_float: srm as a float.
    :type srm_float: float
    :return: hex color
    :rtype: str
    """
    srm_index = math.floor(srm_float)
    try:
        return HEX_COLORS[srm_index]
    except IndexError:
        return HEX_COLORS[-1]
