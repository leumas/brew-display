"""Tap model."""

from brewdisplay.extensions import db

# pylint: disable=no-member


class Tap(db.Model):
    """Tap model."""
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    tap_number = db.Column(db.Integer, unique=True)
    serving = db.relationship("Recipe", uselist=False, back_populates="on_tap")

    @classmethod
    def add(cls):
        """Add a tap."""
        number_of_taps = cls.query.count()
        new_tap = cls(tap_number=number_of_taps + 1)
        db.session.add(new_tap)
        db.session.commit()
        return new_tap

    @staticmethod
    def delete():
        """Delete a tap."""
        tap = Tap.query.order_by(Tap.tap_number.desc()).first()
        if not tap:
            return False
        db.session.delete(tap)
        db.session.commit()
        return True

    def put_on_tap(self, recipe):
        """Update a tap from incoming data."""
        self.serving = recipe
        self.save()

    def save(self):
        """Save a tap."""
        db.session.add(self)
        db.session.commit()
