"""Rating model for Recipes."""

from brewdisplay.extensions import db

# pylint: disable=no-member


class Rating(db.Model):
    """Rating model for recipe."""
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    recipe_id = db.Column(db.Integer, db.ForeignKey('recipe.id'))
    rating = db.Column(db.Integer)

    def delete(self):
        """Delete self."""
        self.pre_delete()
        db.session.delete(self)
        db.session.commit()

    def persist(self):
        """Persist the recipe to the datastore."""
        db.session.add(self)
        db.session.commit()
