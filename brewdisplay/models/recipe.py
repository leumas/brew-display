"""Recipe model."""

import datetime
import os
import uuid

from flask import current_app
from pybeerxml import Parser

from brewdisplay.extensions import db

from brewdisplay.models.rating import Rating
from brewdisplay.models.tap import Tap

# pylint: disable=no-member


class Recipe(db.Model):
    """Recipe model."""
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String, nullable=False, unique=True)
    beerxml_file = db.Column(db.String, nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.now())
    modified = db.Column(db.DateTime, default=datetime.datetime.now())
    abv = db.Column(db.Float, nullable=True)
    srm = db.Column(db.Float, nullable=True)
    on_tap = db.relationship(Tap, back_populates="serving")
    on_tap_id = db.Column(db.Integer, db.ForeignKey('tap.id'))
    description = db.Column(db.Text)
    ratings = db.relationship(
        Rating, backref="recipe", cascade="all, delete-orphan")

    @property
    def average_rating(self):
        """Determine the average rating of a recipe."""
        number_of_ratings = len(self.ratings)
        if number_of_ratings == 0:
            return 0
        return sum(
            [rating.rating for rating in self.ratings]) / number_of_ratings

    @classmethod
    def create(cls, **kwargs):
        """Create a recipe and commit it."""
        recipe = cls(**kwargs)
        db.session.add(recipe)
        db.session.commit()
        return recipe

    @classmethod
    def from_beerxml(cls, raw_xml, save_as=None):
        """Load a recipe from BeerXML.

        :param raw_xml: The raw XML
        :type raw_xml: String
        :return: brewdisplay.models.recipe.Recipe
        """
        new_recipe = Recipe()
        new_recipe.beerxml_file = save_as or str(uuid.uuid4())

        with open(new_recipe.fullpath, 'w') as input_file:
            input_file.write(raw_xml)

        xml_parser = Parser()
        recipe = xml_parser.parse(new_recipe.fullpath)[0]

        new_recipe.name = recipe.name
        new_recipe.srm = recipe.est_color
        new_recipe.abv = recipe.est_abv

        return new_recipe

    @classmethod
    def load_from_disk(cls, read_dir):
        """Load the recipes that live on disk to the application."""
        xml_parser = Parser()
        recipes = []
        for recipe_xml_path in os.listdir(read_dir):
            recipe_xml = xml_parser.parse(
                os.path.join(read_dir, recipe_xml_path))[0]
            # Is the recipe already in the
            potential_recipe = Recipe.query.filter_by(
                beerxml_file=recipe_xml_path).first()
            if potential_recipe:
                continue
            # If not, create and add.
            recipe = Recipe(
                name=recipe_xml.name,
                beerxml_file=recipe_xml_path,
                abv=recipe_xml.est_abv,
                srm=recipe_xml.est_color)

            db.session.add(recipe)
            db.session.commit()
            recipes.append(recipe)

        return recipes

    @property
    def fullpath(self):
        """The full path for the recipe XML file on disk."""
        read_dir = current_app.config['RECIPE_FOLDER']
        return os.path.join(read_dir, self.beerxml_file)

    def persist(self):
        """Persist the recipe to the datastore."""
        db.session.add(self)
        db.session.commit()

    def pre_delete(self):
        """Perform cleanup for the object."""
        if self.beerxml_file:
            os.remove(self.fullpath)

    def update(self, attr_dictionary):
        """Update the recipe with incoming data."""
        self.name = attr_dictionary.get('name', self.name)
        self.description = attr_dictionary.get('description', self.description)
        self.abv = attr_dictionary.get('abv', self.abv)
        self.srm = attr_dictionary.get('srm', self.srm)
        self.modified = datetime.datetime.now()
        db.session.add(self)
        db.session.commit()

    def delete(self):
        """Delete self."""
        self.pre_delete()
        db.session.delete(self)
        db.session.commit()
