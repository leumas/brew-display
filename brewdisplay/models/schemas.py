"""Schemas for models in brewdisplay.

Schemas are built using flask-marshmallow.
"""
from brewdisplay.extensions import marshmallow
from brewdisplay.models.tap import Tap
from brewdisplay.models.recipe import Recipe

# pylint: disable=too-few-public-methods,no-member


class RecipeSchema(marshmallow.ModelSchema):
    """Schema for brewdisplay.models.recipe.Recipe."""
    class Meta:
        """Meta information for this schema."""
        model = Recipe

    _permalink = marshmallow.UrlFor(
        'api_v1_recipes.RecipeById', recipe_id='<id>', _external=True)


class TapSchema(marshmallow.ModelSchema):
    """Schema for brewdisplay.models.tap.Tap."""
    class Meta:
        """Meta information for this schema."""
        model = Tap

    _permalink = marshmallow.UrlFor(
        'api_v1_taps.TapsByID', tap_id='<id>', _external=True)
    serving = marshmallow.Nested(RecipeSchema)


class RecipeQueryArgsSchema(marshmallow.Schema):
    """Schema for the query argument."""
    class Meta:
        """Meta information for this schema."""
        strict = True

    name = marshmallow.String()
