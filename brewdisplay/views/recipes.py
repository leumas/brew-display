from flask import Blueprint, request

from brewdisplay.models.recipe import Recipe
from brewdisplay.models.tap import Tap
from brewdisplay.extensions import db

blueprint = Blueprint('recipes-views', __name__, url_prefix='/views/recipes')


@blueprint.route('/upload', methods=['POST'])
def upload_post():
    """Process an uploaded file through the web UI."""
    description = request.form.get('description')
    assert len(request.files) == 1

    # TODO buffer this to disk
    raw_xml = request.files['recipe-xml'].read()
    new_recipe = Recipe.from_beerxml(raw_xml.decode())
    new_recipe.description = description
    which_tap = request.form.get('on-tap', None)
    # If the tap was supplied, put it on it
    # Can we have multiple beers from the same tap?
    # No.
    if which_tap:
        new_recipe.on_tap = Tap.query.filter_by(
            tap_number=which_tap).first_or_404()
    db.session.add(new_recipe)
    db.session.commit()

    return description
