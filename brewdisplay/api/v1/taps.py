"""Api endpoints for Tap model."""

from brewdisplay.models.schemas import TapSchema
from brewdisplay.models.tap import Tap
from brewdisplay.models.recipe import Recipe
from flask.views import MethodView
from flask_rest_api import Blueprint

blueprint = Blueprint(
    'api_v1_taps', 'taps', url_prefix='/api/v1/taps',
    description='Operations on taps')


@blueprint.route('/')
class Taps(MethodView):
    @blueprint.response(TapSchema(many=True))
    def get(self):
        """Get Taps."""
        return Tap.query.all()

    @blueprint.response(TapSchema, code=201)
    def post(self):
        """Create a tap."""
        return Tap.add()

    @blueprint.response(code=204)
    def delete(self):
        """Delete a tap."""
        result = Tap.delete()
        return result


@blueprint.route('/<tap_id>')
class TapsByID(MethodView):
    @blueprint.response(TapSchema)
    def get(self, tap_id):
        """Retrieve a tap by ID."""
        return Tap.query.get_or_404(tap_id)


@blueprint.route('/<tap_id>/serves/<recipe_id>')
class ServeRecipeOnTap(MethodView):
    @blueprint.response(TapSchema)
    def put(self, tap_id, recipe_id):
        """Edit a tap, which means put a recipe on it."""
        tap = Tap.query.get_or_404(tap_id)
        recipe = Recipe.query.get_or_404(recipe_id)
        tap.put_on_tap(recipe)
        return tap


@blueprint.route('/<tap_id>/serves/')
class RecipesServes(MethodView):
    @blueprint.response(code=204)
    def delete(self, tap_id):
        """Remove a taps serving."""
        tap = Tap.query.get_or_404(tap_id)
        tap.serving = None
        tap.save()
        return True
