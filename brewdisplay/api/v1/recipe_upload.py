from flask import (
    request,
    Blueprint,
    url_for,
    redirect
)

from brewdisplay.extensions import db
from brewdisplay.models.recipe import Recipe


blueprint = Blueprint(
    'api_v1_recipes_upload',
    'recipe_upload',
    url_prefix='/api/v1/recipes',
)


@blueprint.route('/beerxml', methods=['POST'])
def recipe_create_from_xml():
    """Upload a beerXML file."""
    assert 'file' in request.files and len(request.files) == 1

    # TODO buffer this to disk
    raw_xml = request.files['file'].read()
    new_recipe = Recipe.from_beerxml(raw_xml.decode())
    # which_tap =  request.form.get('on-tap', None)
    # If the tap was supplied, put it on it
    # Can we have multiple beers from the same tap?
    # No.
    # if which_tap:
    #     new_recipe.on_tap = Tap.query.filter_by(
    #         tap_number=which_tap).first_or_404()
    db.session.add(new_recipe)
    db.session.commit()
    return redirect(url_for(
        'api_v1_recipes.RecipeById',
        recipe_id=new_recipe.id))
