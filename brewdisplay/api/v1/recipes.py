from flask import (
    current_app,
    send_from_directory,
)
from flask.views import MethodView
from flask_rest_api import Blueprint

from brewdisplay.extensions import db
from brewdisplay.models.recipe import Recipe
from brewdisplay.models.rating import Rating
from brewdisplay.models.schemas import RecipeSchema

blueprint = Blueprint(
    'api_v1_recipes', 'recipes', url_prefix='/api/v1/recipes',
    description='Operations on recipes')


@blueprint.route('/')
class Recipes(MethodView):
    @blueprint.response(RecipeSchema(many=True))
    def get(self):
        """Return a collection of recipes."""
        return Recipe.query.all()

    @blueprint.arguments(RecipeSchema)
    @blueprint.response(RecipeSchema, code=201)
    def post(self, new_recipe):
        """Create a new recipe."""
        new_recipe.persist()
        return new_recipe


@blueprint.route('/<recipe_id>')
class RecipeById(MethodView):
    @blueprint.response(RecipeSchema)
    def get(self, recipe_id):
        """Retreive a recipe by ID."""
        return Recipe.query.get_or_404(recipe_id)

    @blueprint.arguments(RecipeSchema)
    @blueprint.response(RecipeSchema)
    def put(self, update_data, recipe_id):
        """Update an existing recipe by ID."""
        update_data.persist()
        return update_data

    @blueprint.response(code=204)
    def delete(self, recipe_id):
        """Delete a recipe by ID."""
        recipe = Recipe.query.get_or_404(recipe_id)
        recipe.delete()


@blueprint.route('/<recipe_id>/beerxml')
class RecipeDownload(MethodView):
    def get(self, recipe_id):
        """Download the raw beerxml of a recipe.

        :param recipe_id: ID of the desired recipe.
        :type recipe_id: int
        """
        recipe = Recipe.query.get_or_404(recipe_id)
        read_dir = current_app.config['RECIPE_READ_FOLDER']
        return send_from_directory(
            read_dir,
            recipe.beerxml_file,
            as_attachment=True,
            attachment_filename=recipe.name + '.xml')


@blueprint.route('/<recipe_id>/rate/<rating>')
class RecipeRatings(MethodView):
    @blueprint.response(code=201)
    def post(self, recipe_id, rating):
        """Rate a recipe.

        :param recipe_id: ID of the desired recipe.
        :type recipe_id: int
        """
        recipe = Recipe.query.get_or_404(recipe_id)
        rating = Rating(recipe_id=recipe_id, rating=rating)
        recipe.ratings.append(rating)
        db.session.add(recipe)
        db.session.commit()
        return {}
