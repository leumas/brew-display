"""House of configurations for Brewdisplay."""

# pylint: disable=too-few-public-methods


class Config:
    """Base config."""
    TESTING = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///brew-display.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # There is odd pathing here
    RECIPE_FOLDER = './recipes/'
    RECIPE_READ_FOLDER = '../recipes/'
    debug = True
    STATIC_DIR = ''
    DEV = 'dev'
    OPENAPI_VERSION = '3.0.2'


class TestConfig(Config):
    """Base Config for TESTING."""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    RECIPE_FOLDER = '/tmp/recipes'
    RECIPE_READ_FOLDER = '/tmp/recipes'
    SERVER_NAME = 'localhost:5000'
