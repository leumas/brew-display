"""Flask extensions for the brewdisplay."""

# pylint: disable=invalid-name

from flask_sqlalchemy import SQLAlchemy
from flask_rest_api import Api
from flask_marshmallow import Marshmallow

api = Api()
db = SQLAlchemy()
marshmallow = Marshmallow()
